<?php
/**
 *
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    WIO_WP
 * @subpackage WIO_WP/includes
 */
/**
 * The get_option functionality of the plugin.
 *
 *
 * @package    WIO_WP
 * @subpackage WIO_WP/includes
 * @author     Your Name <email@example.com>
 */


class WIO_WP_Option {

	/**
	 * Get an option
	 *
	 * Looks to see if the specified setting exists, returns default if not.
	 *
	 * @since 	1.0.0
	 * @return 	mixed 	$value 	Value saved / $default if key if not exist
	 */
	static public function get_option( $key, $default = false ) {

		if ( empty( $key ) ) {
			return $default;
		}

		// @TODO: change plugin_name_settings
		$plugin_options = get_option( 'wio_settings', array() );

		$value = isset( $plugin_options[ $key ] ) ? $plugin_options[ $key ] : $default;

		return $value;
	}
}
