<?php

class WIO{

	//default strings
	// public $scan_timeout = 43200000;
	public $scan_timeout = 43200;
	public $scan_txt = "1 PER 12 HRS";

	//vars
	public $authed;
	public $creds;
	public $has_subscription;
	public $authed_site;
	public $plugin_dir;

	private $client_data = array();
	private $ip;
	private $site;
	private $token;
	private $secret;
	private $site_secret;
	private $call_rtn;

	//constructor
	function __construct($token,$secret,$site_secret){
		date_default_timezone_set('America/Los_Angeles');

		$this->_BKUPDIR = ABSPATH."wp-content/uploads/wio-backup/_imgs/";
		$this->_DBLOC = ABSPATH."wp-content/uploads/wio-backup";

		if(!file_exists($this->_DBLOC)){
			mkdir($this->_DBLOC);
		}
		if(!file_exists($this->_BKUPDIR)){
			mkdir($this->_BKUPDIR);
		}

		if(!$this->is_writable_dir($this->_DBLOC)){
			chmod($this->_DBLOC,0777);
		}
		if(!$this->is_writable_dir($this->_BKUPDIR)){
			chmod($this->_BKUPDIR,0777);
		}

		$this->endpoint = "https://app.webimageoptimizer.com/_api/";
		$this->plugin_dir = end(explode("/",dirname(__DIR__)));


		$this->ip = $_SERVER["SERVER_ADDR"];
		$this->site = $_SERVER["HTTP_HOST"];
		$this->protocol = isset($_SERVER['HTTPS'])?"https":"http";
		$this->email = "test@webimageoptimizer.com";
		$this->token = $token;
		$this->secret = $secret;
		$this->site_secret = $site_secret;

		$this->auth();
	}

	//methods
	public function connect_db($db = null){

		if(!isset($db)){
			$db = $this->site;
		}else{
			$db = $this->site.$db;
		}

		require_once(__DIR__."/lib/fllat/fllat.php");

		$db_name = sha1($db);
		//
		// $db_file = $this->_DBLOC."/".$db_name.".dat";
		// if(!file_exists($db_file)){
		// 	touch($db_file);
		// 	chmod($db_file, 0777);
		// }

		$this->db = new Fllat($db_name,$this->_DBLOC);

		return $this->db;
	}

	private function api_call($section = null,$action = null,$data = null){
		//extract data from the post

		//set POST variables
		$url = $this->endpoint;

		// Get cURL resource

		$rtn = array(
			"data"=>"here"
		);


        $params = "_sec=".$this->secret
        		."&_ssec=".$this->site_secret
        		."&_ip=".$this->ip
        		."&_wpd=".$this->plugin_dir
        		."&_tkn=".$this->token
        		."&_act=".$action
        		."&_sect=".$section;

        if(isset($data)){
        	$params = $params."&".http_build_query($data);
        }

		if($section == "img"){
			$rtn = file_get_contents($url."?".$params);
			$rtn = json_decode($rtn,true);

			return $rtn;
		}

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url); //Remote Location URL
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 7); //Timeout after 7 seconds
		curl_setopt($ch, CURLOPT_HEADER, 0);

		//We add these 2 lines to create POST request
		curl_setopt($ch, CURLOPT_POST, count($params)); //number of parameters sent
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params); //parameters data

		$result = curl_exec($ch);
		$rtn = $result;
		curl_close($ch);

		return json_decode($rtn);
	}

	public function test_call(){
		return $this->get_info();
	}

	public function test($attachment_id){
		session_start();

		if($this->opt_image($attachment_id)){
			$img["ok"] = true;
		}else{
			$img["call_rtn"] = $this->call_rtn;
		}
		$_SESSION["img_uploaded"] = $img;
		$_SESSION["wio_authed"] = $this->authed;
	}

//authentication methods

	private function auth(){

		//authenticate
		$auth = $this->api_call("auth","api_auth");

		if($auth->authd == true){
			$resp = array(
				"authed"=>1
			);

			if($auth->has_subscription == true){
				$this->has_subscription = true;
			}else{
				$this->has_subscription = false;
			}
			$this->authed = true;
			return $resp;
		}else{
			$error = array(
				"error"=>true,
				"msg"=>"Authentication failed"
			);
			return $error;
		}

	}

//logging methods
	function mk_log($log_data){
		//
		//write to log to local db
		//
		//@param $log_data @array
		//

		$log = $log_data;

		$log["time"] = strtotime("now");

		$db = $this->connect_db("activity_log");

		$db->add($log);

		return true;
	}

	function clean_logs($full = false){
		$db = $this->connect_db("activity_log");

		if($full === false){
			$db->del();
		}else{
			$logs = $db->select();
			$keep = array();
			$c = 0;
			while($c < 5){
				$keep[] = $logs[$c];
				$c++;
			}
			$db->rw($keep);
		}
		return true;
	}

	function get_logs($limit = 5,$json = null){
		$db = $this->connect_db("activity_log");
		$list_o_logs = $db->select();
		if(count($list_o_logs) < 1){
			return array();
		}
		$logs = array_reverse($list_o_logs);
		$rtn = array();
		$c = 0;
		$time = strtotime("now");
		if($limit == 0){
		    $limit = count($logs);
		}
		while($c < $limit && $c < count($logs)){
			$log = $logs[$c];

			//get how long ago
			$ago = (int)($time - $log["time"]);
			$ago_label = ($ago < 2)?" sec ago":" secs ago";

			if($ago > 60){
				$ago = (int)$ago/60;
				$ago_label = ($ago < 2)?" min ago":" mins ago";

				if($ago > 60){
					$ago = (int)$ago/60;
					$ago_label = ($ago < 2)?" hr ago":" hrs ago";

					if($ago > 24){
						$ago = (int)$ago/24;
						$ago_label = ($ago < 2)?" day ago":" days ago";
					}
				}
			}

			$log["ago"] = floor($ago).$ago_label;
			$rtn[] = $log;
			$c++;
		}
		if($json){
		    return json_encode($rtn);
		}
		return $rtn;
	}

//client account methods
	function subscription_display(){

		$display = "<h5 class='cash' style='margin-bottom:.5em;padding-bottom:0;font-weight:300;'>SUBSCRIPTION ACTIVE</h5>";

		$display_inactive = "<h5 style='margin-bottom:.5em;padding-bottom:0;font-weight:300;'>SUBSCRIPTION INACTIVE</h5>";

		if($this->has_subscription){
			echo $display;
		}else{
			echo $display_inactive;
		}
	}


	//get methods
	function get_client_info(){
		$rtn = $this->api_call("client","get");
		return $rtn;
	}

	function get_creds(){
		$creds = $this->get_client_info()->creds;
		return $creds;
	}

	function get_site_info(){
		$site_info = $this->get_client_info()->site;
		return $site_info;
	}
	function grab_image($wp_id){
		$xdata = array(
			"_wpid"=>$wp_id
		);
		$api_response = $this->api_call("img","grab",$xdata);

		return $api_response;

		if(!isset($api_response["ok"])){
			return $api_response;
		}else{
			return $api_repsonse;
		}
	}
	function get_images($wpdb){
		//grab images from WP DB
		$imgs = $wpdb->get_results("SELECT * FROM {$wpdb->posts} WHERE post_mime_type LIKE 'image%'");

		//create scan array
		$scan = array(
			"count"=>0,
			"uri"=> wp_upload_dir()["baseurl"],
			"imgs"=>array()
		);

		//loop through images
		foreach($imgs as $img){

			$img_id = (int)$img->ID;

			$optimized = get_post_meta($img_id,'optimized');

			if($optimized[0] == 1){
				//already optimized
			}else{
				//add to scan list
				$scan["imgs"][] = array(
					"_id"=>(int)$img->ID,
					"_uri"=>WIO::get_short_uri($img->guid)
				);
				$scan["count"]++;
			}
		}

		return $scan;
	}

	function request_scan(){
		if($this->authed == true){
			$api_response = $this->api_call("scan","req",array());

			if($api_response->req == true){
				$log = array(
					"action"=>"Site Image Scan Request",
					"domain"=>$this->get_site_info()->url
				);

				$this->mk_log($log);
				$rtn = true;
			}else{
				$rtn = false;
			}
		}


		return $api_response;
	}

	//buy credits
	function buy_creds($amount){

		if($this->authed == true){
			$call_data = array(
				"_amt"=>(int)$amount
			);
			$api_response = $this->api_call("xact","mk",$call_data);

			if($api_response->added == true){
				$rtn = true;
			}else{
				return false;
			}
		}
		return $api_response;
	}

//Image opt methods

	function img_to_b64($path){
		//image to base64

		return $b64;
	}
	public function b64_to_img($b64,$path,$raw = null){
		//
		//@param $b64 - base64 string
		//@param $path - path to save image. set null for raw data return
		//@param $raw - return raw decoded data
		//
		//b64 to image

		if(!isset($raw)){
			$ifp = fopen($path, "wb");
			if(fwrite($ifp, base64_decode($b64))){
				fclose($ifp);
				$rtn = true;
			}else{
				$rtn = false;
			}

			return $rtn;
		}else{
			return $data;
		}
	}


	function replace_image_ftp($host,$ftp_creds,$img){

	}

	function save_opt_img($img_b64,$new_path){

	}

	function put_opt_image($img_b64,$img_path){
		//put optimized image in path
		//
		//@param $img_b64 - base64 string
		//@param $img_path - path to save image. set null for raw data return
		//
		//@return boolean
		//
		return $this->b64_to_img($img_b64,$img_path);
	}

	function opt_image($attachment_id){
		//
		//send image to API to be optimized
		//
		//@param $attachment_id - wordpress attachement id for image
		//@param $web_path - web path of image
		//@param $ab_path - absolute path of image on server
		//
		//@return boolean


		if($this->authed == true && $this->get_client_info()->status == 1){
			$data  = wp_get_attachment_image_src( $attachment_id );
			$img_web_path = $data[0];
			$img_bits = explode("wp-content", $data[0] );
			$name_bits = explode("/", $data[0] );
			$img_name = $name_bits[(count($name_bits) - 1 )];
			$img_path = ABSPATH."wp-content".$img_bits[1];


			$img_bk_path = $this->_BKUPDIR."{$attachment_id}-$img_name";
			$new_path = explode($img_name,$data[0])[0]."{$img_name}";
			$img = array(
				"test"=>$new_path
			);


			$call_data = array(
				"_impath"=>$img_web_path,
				"_imnewpath"=>WIO::get_img_short_abs($img_web_path),
				"_url"=>wp_upload_dir()["baseurl"],
				"_imname"=>$img_name,
				"_imaid"=>$attachment_id
			);

			//send image to api
			$api_response = $this->api_call("opt","imize",$call_data);

			if($api_response->ok === true){
				$log = array(
					"action"=>"Image Added To Queue",
					"url"=>$img_web_path,
					"size"=>filesize($img_bk_path)
				);

				$this->mk_log($log);
			}

			$rtn = $api_response;
			return $rtn;
		}else{
			return;
		}
	}

	function update_opt_image($wp_id,$uri,$path,$bkup,$size){
		$db = $this->connect_db();

		if(!isset($size)){
			$size = "size fail update_opt_image";
		}

		$img = array(
			"wp_id"=>(int)$wp_id,
			"uri"=>$uri,
			"path"=>$path,
			"bkup"=>$bkup,
			"opt"=>true,
			"time_opt"=>date("D M j,Y G:i T"),
			"size"=>$size
		);

		global $wpdb;

		if(!$db->exists("wp_id",$wp_id) && $this->check_bkups($wpdb)){
			$db->add($img);

			$log = array(
				"action"=>"Image Optimized",
				"url"=>$uri,
				"size"=>$size
			);
			$this->mk_log($log);
		}
	}

	public function get_bkup_list($json_encode){
		$db =  $this->connect_db();

		if($json_encode){
			return json_encode($this->array_values_recursive($db->where(array(),"hide",null)));
		}else{
			return $db->select();
		}


	}

	public function get_full_bkup_list($json_encode){
		$db =  $this->connect_db();

		if($json_encode){
			return json_encode($this->array_values_recursive($db->select()));
		}else{
			return $db->select();
		}
	}

	public function array_values_recursive( $array ) {
		$array = array_values( $array );
		for ( $i = 0, $n = count( $array ); $i < $n; $i++ ) {
			$element = $array[$i];
			if ( is_array( $element ) ) {
			    $array[$i] = $this->array_values_recursive( $element );
			}
		}
		return $array;
	}

	public function backup_img($img_path,$img_bk_path){
		if(!file_exists($this->_BKUPDIR)){
			mkdir($this->_BKUPDIR);
		}

		if(copy($img_path,$img_bk_path)){
			return true;
		}else{
			return false;
		}
	}

	public function restore_backup($bkup_path){
		$db = $this->connect_db();
		$bkimg = explode("-",$bkup_path);

		$id = $bkimg[0];
		$img_name = $bkimg[1];

		$bkup = $db->where("wp_id",$id);


	}

	public function check_bkups($wpdb){
		if($this->get_settings($wpdb)["backups"] == 0){
			return true;
		}else{
			return false;
		}
	}

	public function get_bkup_dir_size(){
		$dir = $this->_BKUPDIR;
		$count_size = 0;
		$count = 0;
		$dir_array = scandir($dir);
		  foreach($dir_array as $key=>$filename){
		    if($filename!=".." && $filename!="."){
		       if(is_dir($dir."/".$filename)){
		          $new_foldersize = foldersize($dir."/".$filename);
		          $count_size = $count_size+ $new_foldersize;
		        }else if(is_file($dir."/".$filename)){
		          $count_size = $count_size + filesize($dir."/".$filename);
		          $count++;
		        }
		   }
		 }
		return $this->get_file_size(null,$count_size);
	}

	function get_file_size($file,$size = null){
		  if(!isset($size)){
			  $size = filesize($file);
		  }
	      $sizes = array(" Bytes", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB");
	      if ($size == 0) { return('n/a'); } else {
	      return (round($size/pow(1024, ($i = floor(log($size, 1024)))), 2) . $sizes[$i]); }
	}

	public function get_settings($wpdb){

		$option = $wpdb->get_results('SELECT * FROM '. $wpdb->prefix.'options where option_name = \'wio_settings\' ');

		$check_data = unserialize($option[0]->option_value);

		return $check_data;
	}

	public function get_img_short_abs($uri){
		$img_web_path = $uri;
		$img_bits = explode("uploads", $uri );
		$name_bits = explode("/", $uri );
		$img_name = $name_bits[(count($name_bits) - 1 )];

		$new_path = explode($img_name,$img_bits[1])[0].$img_name;

		return $new_path;
	}

	public function get_short_uri($uri){
		$img_bits = explode("uploads", $uri );

		return $img_bits[1];
	}

	public function is_writable_dir($dir){
		$test_file = "{$dir}/test.wio";

		touch($test_file);

		if(file_exists($test_file)){
			if(unlink($test_file)){
				return true;
			}else{
				return false;
			}
		}
	}

	public function update_img_wp($attatchment_id, $img, $img_path){
		if(update_attached_file( $attachment_id, $img )){
		  $attach_data = wp_generate_attachment_metadata( $attachment_id, $img_path );
		  $attach_data->optimized = 1;
		  if(wp_update_attachment_metadata( $attachment_id,  $attach_data )){
		  	return true;
		  }else{
		  	return false;
		  }
		}
	}
	public function post_without_wait($url, $params){
	    foreach ($params as $key => &$val) {
	      if (is_array($val)) $val = implode(',', $val);
	        $post_params[] = $key.'='.urlencode($val);
	    }
	    $post_string = implode('&', $post_params);

	    $parts=parse_url($url);

	    $fp = fsockopen($parts['host'],
	        isset($parts['port'])?$parts['port']:80,
	        $errno, $errstr, 30);

	    $out = "POST ".$parts['path']." HTTP/1.1\r\n";
	    $out.= "Host: ".$parts['host']."\r\n";
	    $out.= "Content-Type: application/x-www-form-urlencoded\r\n";
	    $out.= "Content-Length: ".strlen($post_string)."\r\n";
	    $out.= "Connection: Close\r\n\r\n";
	    if (isset($post_string)) $out.= $post_string;

	    fwrite($fp, $out);
	    fclose($fp);
	}
}
