<?php

	switch($sub_action){
		case "mv":
			// require_once( __DIR__ . '/../../../../../wp-load.php' );

			$upload_dir = wp_upload_dir();

			$upload_dir = $upload_dir["basedir"];

			$img_path = explode("-",$_REQUEST["_ii"]);
			$bk_name =$_REQUEST["_ii"];

			$img_id = (int)$img_path[0];

			$wio = new WIO($token,$secret,$site_secret);

			$db = $wio->connect_db();

			//get image from backup db
			$img = $db->get("path","wp_id",$img_id);
			$img_db_index = $db->index("wp_id",$img_id);

			//copy image back to orginal place
			$original_path = $upload_dir.$img;
			$backup_path = $upload_dir."/wio-backup/_imgs/".$bk_name;


			if(copy($backup_path,$original_path)){

			//regenerate meta(thumbnails & that)

				$post = get_post($img_id);
				$file = get_attached_file($img_id);

				update_attached_file( $img_id, $original_path );

			//save meta
				include( ABSPATH . 'wp-admin/includes/image.php' );
				  $attach_data = wp_generate_attachment_metadata( $img_id, $original_path );
				  wp_update_attachment_metadata( $img_id,  $attach_data );


			//update to not show in backup_list from backup db

				$update = $db->update($img_db_index,array("hide"=>true));

				$rtn = array(
					"ok"=>true,
					"restored"=>true,
					"img"=>$original_path
				);
			}else{
				$rtn = array(
					"err"=>true,
					"msg"=>"Failed to restore backup"
				);
			}
		break;
		case "rm":
			require_once( __DIR__ . '/../../../../../wp-load.php' );

			$upload_dir = wp_upload_dir();

			$upload_dir = $upload_dir["basedir"];

			$img_path = explode("-",$_REQUEST["_ii"]);
			$bk_name =$_REQUEST["_ii"];

			$img_id = (int)$img_path[0];

			$wio = new WIO($token,$secret,$site_secret);

			$db = $wio->connect_db();

			//get image from backup db
			$img = $db->get("path","wp_id",$img_id);
			$img_db_index = $db->index("wp_id",$img_id);
			$backup_path = $upload_dir."/wio-backup/_imgs/".$bk_name;


			if(unlink($backup_path)){

			//update to not show in backup_list from backup db

				$update = $db->rm($img_db_index);

				$rtn = array(
					"ok"=>true,
					"deleted"=>true,
				);
			}else{
				$rtn = array(
					"err"=>true,
					"msg"=>"Failed to delete backup"
				);
			}
		break;
	}
