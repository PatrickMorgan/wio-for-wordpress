<?php
/**
 * Provide a meta box view for the settings page
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/admin/partials
 */

/**
 * Meta Box
 *
 * Renders a single meta box.
 *
 * @since       1.0.0
*/
?>
<div class="clearfix" id="checkout">
	
	<?php
	if($wio->authed){
	//display section if authenticated
	?>
	
	<div class="huge_form">
		<form>
			<div id="pay_top">
				<h2>I WANT TO BUY</h2>
				<input id="cred_val" type="number" min="100" class="huge_input" placeholder="Min 100" value="100">
				<h2>CREDITS</h2>
				<hr>
			</div>
			<div id="pay_btn_holder">
				<h1><button title="" id="buy_btn" class="txt-btn pay-btn" data-original-title="Checkout">PAY <span id="cred_amount" class="cash">$5.00</span> <i class="fa fa-angle-right"></i></button></h1>
			</div>
		</form>
	</div>
	<?php
	}else{
		//or not
		include(__DIR__."/wio-not-authed-display.php");
	}
	?>
</div>
