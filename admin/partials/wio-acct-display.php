<?php
/**
 * Provide a meta box view for the settings page
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/admin/partials
 */

/**
 * Meta Box
 *
 * Renders a single meta box.
 *
 * @since       1.0.0
*/
?>
<?php
	//vars
	
	
?>
<div class="clearfix" id="acct">
			<?php
			if($wio->authed){
				$client = $wio->get_client_info();
			?>
			<div class="top">
				<h2 class=""><?php echo $client->email; ?></h2>
				<hr>
			</div>
			<div class="bottom">
				
				<?php $wio->subscription_display(); ?>
			</div>
			<?php
			}else{
				include(__DIR__."/wio-not-authed-display.php");
			}
			
			?>
	
</div>
