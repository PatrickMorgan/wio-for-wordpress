<?php
/**
 * Provide a meta box view for the settings page
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/admin/partials
 */

/**
 * Meta Box
 *
 * Renders a single meta box.
 *
 * @since       1.0.0
*/
?>
<?php
	//vars
	global $wpdb;
		//bootleg stylesheet load ;)
	echo "<style>";
	require(__DIR__."/../js/bower_components/datatables/media/css/jquery.dataTables.min.css");
	echo "</style>";

	$config_link = admin_url('admin.php?page=wio&tab=api_config');
?>
<?php

	if($wio->check_bkups($wpdb)){
		?>
		<a href="<?php echo $config_link; ?>" id="" class="btn wio_msgs win first link_btn"><p class="msg-txt"><span class="dark-txt">BACKUPS</span> ENABLED</p></a>
		<?php
	}else{
		?>
		<a href="<?php echo $config_link; ?>" id="" class="btn wio_msgs first link_btn"><p class="msg-txt "><span class="dark-txt">BACKUPS</span> DISABLED</p></a>
		<?php
	}

?>
<div class="btn wio_msgs info" >
	<p class="msg-txt">TOTAL STORAGE USED BY IMAGE BACKUPS: <span class="dark-txt"><?php echo $wio->get_bkup_dir_size(); ?></span></p>
</div>
<div class="clearfix" id="backups">
			<?php
			if($wio->authed){
				$client = $wio->get_client_info();
				$site = $client->site;

			?>

		            <div class="table-responsive">
				        <table class="table" id="backup_list">

				        </table>
				    </div>
			<?php

				//get list data
				echo "<script type='text/javascript'>";
				echo "var bkUpList = ".$wio->get_bkup_list(true).";";
				echo "</script>";

				//load datatables
				echo "<script type='text/javascript'>";
				require_once(__DIR__."/../js/bower_components/datatables/media/js/jquery.dataTables.min.js");
				echo "</script>";

			}else{
				include(__DIR__."/wio-not-authed-display.php");
			}

			?>

</div>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/0.5.0/sweet-alert.min.js"></script>
