<?php
/**
 * Provide a dashboard view for the plugin
 *
 * This file is used to markup the plugin settings page.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/admin/partials
 */

/**
 * Options Page
 *
 * Renders the settings page contents.
 *
 * @since       1.0.0
*/

?>
<?php
	//WIO Object

	$wio_config = get_option('wio_settings');
	if(isset($wio_config["api_token"]) && isset($wio_config["api_secret"]) && isset($wio_config["site_secret"])){
		$wio = new WIO($wio_config["api_token"],$wio_config["api_secret"],$wio_config["site_secret"]);
		$site = $wio->get_site_info();
	}


	//vars
	$url_bits = explode("/",$site->url);
	$site_url = $site->url;
	if(end($url_bits) != "/"){
		$site_url = $site_url."/";
	}


	//check if dirs are writable

	$upload_dir = wp_upload_dir();

	$upload_loc = $upload_dir["baseurl"];
	$upload_dir = $upload_dir["basedir"];

	$write = WIO::is_writable_dir($upload_dir);

	if($site->subscription_type == 1){
		$no_ad = "no-ad";
	}

	//test writing of upload dir
	if($write){
		$rw = 1;
	}else{
		$rw = 0;
	}

?>
<style>
	<?php
		//bootleg stylesheet load ;)
		 require(__DIR__."/../css/wio-admin.css");
		 require(__DIR__."/../css/sweet-alert.min.css");

	 ?>
	 .msg-txt{
		 font-size: 13px !important;
	 }
	 .wio_msgs{
		 padding-top: 0 !important;
	 }
</style>
<div class="wio wrap <?php echo $no_ad; ?>">
	<h2><?php echo esc_html( get_admin_page_title() ); ?> for WP <small>v<?php echo $this->version; ?></small></h2>

	<?php if($wio->authed){
		?>
		<div id="wio_msgs" class="win">
			<p class="msg-txt"><span class="dark-txt">WIO ACCOUNT</span> CONNECTED</p>
		</div>
		<?php
	}else{
		?>
		<div id="wio_msgs" class="err">
			<p class="msg-txt"><span class="dark-txt">WIO ACCOUNT</span> NOT CONNECTED</p>
		</div>
		<?php
	} ?>


	<?php
	if($rw == 0){
		?>
		<div id="perms_msgs" title="No Write Permissions For Uploads" class="err">
			<p class="msg-txt"><span class="dark-txt">UPLOAD PERMISSIONS</span> FAILED</p>
		</div>
		<?php
	}
	?>
	<?php settings_errors( $this->plugin_name . '-notices' ); ?>

	<h2 class="nav-tab-wrapper">
		<?php
		foreach( $tabs as $tab_slug => $tab_name ) {

			$tab_url = add_query_arg( array(
				'settings-updated' => false,
				'tab' => $tab_slug
				) );

			$active = $active_tab == $tab_slug ? ' nav-tab-active' : '';

			echo '<a href="' . esc_url( $tab_url ) . '" title="' . esc_attr( $tab_name ) . '" class="nav-tab' . $active . '">';
			echo esc_html( $tab_name );
			echo '</a>';

			$tab_links[strtolower(str_replace(" ","_",$tab_name))] = $tab_url;
		}
		?>
	</h2>
	<?php

		if($active_tab != "api_config"){
			switch($active_tab){
				case "acct_detail":
					include(__DIR__."/wio-acct-display.php");
				break;
				case "subscription":
					include(__DIR__."/wio-acct-display.php");
				break;
				case "backup":
					include(__DIR__."/wio-backup-display.php");
				break;
				case "dashboard":
					include(__DIR__."/wio-dashboard-display.php");
				break;
				case "help":
					include(__DIR__."/wio-help-display.php");
				break;
				case "wio_logs":
					include(__DIR__."/wio-logs-display.php");
				break;
				default:

				break;
			}
		}else{
	?>
	<?php
	//site info
	if($wio->authed){
	?>
		<div id="acct_msg" class="wio_msgs info first">
			<p class="msg-txt"><span class="dark-txt">URL</span> <?php echo strtoupper($site->url); ?></p>
		</div>
	<?php

		if($wio->has_subscription){
		?>
		<div id="acct_msg" class="wio_msgs win">
			<p class="msg-txt"><span class="dark-txt">SUBSCRIPTION</span> ACTIVE</p>
		</div>
		<?php
		}
	}
	?>
	<div id="poststuff">

		<div id="post-body" class="metabox-holder">


				<div id="postbox-container" class="postbox-container">
				<?php
					do_meta_boxes( $this->snake_cased_plugin_name . '_settings_' . $active_tab, 'normal', $active_tab );
				?>

				</div><!-- #postbox-container-->

		</div><!-- #post-body-->

	</div><!-- #poststuff-->
	<?php

		}
	?>
</div><!-- .wrap -->
<?php
	if($site->subscription_type == 2){
?>
	<div id="add_wall" class="add-wall">
		<a target="_blank" class="banner-link" href ="https://app.webimageoptimizer.com/ads/?_----------_-_-_-_=^">
			<img src="https://app.webimageoptimizer.com/ads/?_----------_-_-_-_=@" />
		</a>
	</div>
<?php
	}
?>
<script type="text/javascript">

	<?php
		//bootleg js load ;)
		//enqueue deez nutz

		$tabs_js_acl = array(
			"backup","dashboard","wio_logs"
		);

		if(in_array($active_tab,$tabs_js_acl)){
			require(__DIR__."/../js/wio-admin.js");

			switch($active_tab){
				case "dashboard":
					if(isset($scan_ok)){
						require(__DIR__."/../js/wio-admin-dashboard.js");
					}
				break;
				case "backup":
					require(__DIR__."/../js/wio-admin-bkup.js");
				break;
				case "wio_logs":
					require(__DIR__."/../js/wio-admin-logs.js");
				break;
			}
			?>
				WIO._tkn = "<?php echo $wio_config["api_token"]; ?>";
				WIO._sec = "<?php echo $wio_config["api_secret"]; ?>";
				WIO._ssec = "<?php echo $wio_config["site_secret"]; ?>";
				WIO._url = "<?php echo $site_url; ?>";
				WIO._uloc = "<?php echo $upload_loc; ?>";
				WIO._loc = "./../wp-content/plugins/<?php echo $wio->plugin_dir; ?>/_js-data/";
			<?php
		}

		require(__DIR__."/../js/sweet-alert.min.js");
	 ?>
</script>
