<?php
/**
 * Provide a meta box view for the settings page
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/admin/partials
 */

/**
 * Meta Box
 *
 * Renders a single meta box.
 *
 * @since       1.0.0
*/
?>
<?php
	//vars
	global $wpdb;
		//bootleg stylesheet load ;)
	echo "<style>";
	require(__DIR__."/../js/bower_components/datatables/media/css/jquery.dataTables.min.css");
	echo "</style>";

	$config_link = admin_url('admin.php?page=wio&tab=api_config');
    $plugin_url = plugin_dir_url(  __FILE__ );
?>
<style>
    .help-topic{
        margin-bottom: 1em;
        margin-top: 1em;
        background-color: #e3e3e3;
        padding: .5em;
        color:
    }
    .help-topic > h1{
        font-size: 36px;
    }
    .help-step > strong{
        font-size: 24px;
    }
    .help-quest{

    }
    .help-quest:active + .help-detail{
        display: block;
    }
    .help-detail{
        /*display: none;*/
    }
    .help-img{
        max-width: 80%;
        height: auto;
		margin: .25em;
    }
</style>
<div class="clearfix" id="help">
	<div class="help-topic">
		<h1 class="help-quest">How do I upgrade my subscription?</h1>
		<div class="help-detail">
			<ul>
				<li class="help-step">
					<strong>Step 1: Go To WIO Panel and login</strong>
					<p>
						<p>Go to <a href="https://app.webimageoptimizer.com">https://app.webimageoptimizer.com</p>
						<img src="<?php echo $plugin_url; ?>../img/login.png" class="help-img" /></a>
					</p>
				</li>
				<li class="help-step">
					<strong>Step 2: Go To  My Website Profiles</strong>
					<p>
						<p>You should be automatically directed to "My Website Profiles", but you can navigate there by selecting "My Website Profiles" from the menu.</p>
						<img src="<?php echo $plugin_url; ?>../img/menu_button.png" class="help-img" /><br />
						<img src="<?php echo $plugin_url; ?>../img/sites.png" class="help-img" />
					</p>
				</li>
				<li class="help-step">
					<strong>Step 4: Click the "Upgrade Subscription" button for the desired site profile.</strong>
					<p>
						<img src="<?php echo $plugin_url; ?>../img/upgrade_button.png" class="help-img" />
					</p>
				</li>
			</ul>
		</div>
	</div>
    <div class="help-topic">
        <h1 class="help-quest">I forgot my password. How do I reset it?</h1>
        <div class="help-detail">
            <ul>
                <li class="help-step">
                    <strong>Step 1: Forget Password</strong>
                    <p>
                        Check.
                    </p>
                </li>
                <li class="help-step">
                    <strong>Step 2: Go To WIO Panel</strong>
                    <p>
                        <p>Go to <a href="https://app.webimageoptimizer.com">https://app.webimageoptimizer.com</p>
                        <img src="<?php echo $plugin_url; ?>../img/login.png" class="help-img" /></a>
                    </p>
                </li>
                <li class="help-step">
                    <strong>Step 3: Request Password Reset</strong>
                    <p>
						<p>Click "Forgot your password?". Enter your email address. Click "REQUEST RESET". Check for an email for Web Image Optimizer.</p>
                        <img src="<?php echo $plugin_url; ?>../img/passwd.png" class="help-img" />
                    </p>
                </li>
                <li class="help-step">
                    <strong>Step 4: Reset Password</strong>
                    <p>
						<p>Follow the link in the email and enter your new password.</p>
                        <img src="<?php echo $plugin_url; ?>../img/passwd_reset.png" class="help-img" />
                    </p>
                </li>
            </ul>
        </div>
    </div>
    <div class="help-topic">
        <h2 class="help-quest">Where can I find the credentials? WIO  account token / WIO account secret / WIO site secret?</h2>
        <div class="help-detail">
            <ul>
                <li class="help-step">
                    <strong>Step 1: Go To WIO Panel and login</strong>
                    <p>
                        <p>Go to <a href="https://app.webimageoptimizer.com">https://app.webimageoptimizer.com</p>
                        <img src="<?php echo $plugin_url; ?>../img/login.png" class="help-img" /></a>
                    </p>
                </li>
                <li class="help-step">
                    <strong>Step 2: Go To  My Website Profiles</strong>
                    <p>
						<p>You should be automatically directed to "My Website Profiles", but you can navigate there by selecting "My Website Profiles" from the menu.</p>
						<img src="<?php echo $plugin_url; ?>../img/menu_button.png" class="help-img" /><br />
                        <img src="<?php echo $plugin_url; ?>../img/sites.png" class="help-img" />
                    </p>
                </li>
                <li class="help-step">
                    <strong>Step 4: Click the "Wordpress Plugin Credentials" button.</strong>
                    <p>
                        <img src="<?php echo $plugin_url; ?>../img/plugin_creds.png" class="help-img" />
                    </p>
                </li>
            </ul>
        </div>
    </div>
	<div class="help-topic">
        <h1 class="help-quest">What is the monthly cost of a WIO account?</h1>
        <div class="help-detail">
            <ul>
                <li class="help-step">
                    <strong>Free for the 1st site. $5 per month for each additional site.</strong>
                </li>
            </ul>
        </div>
    </div>
	<div class="help-topic">
        <h1 class="help-quest">Where can I update my account information?</h1>
        <div class="help-detail">
            <ul>
                <li class="help-step">
                    <strong>Step 1: Go To WIO Panel and login</strong>
                    <p>
                        <p>Go to <a href="https://app.webimageoptimizer.com">https://app.webimageoptimizer.com</p>
                        <img src="<?php echo $plugin_url; ?>../img/login.png" class="help-img" /></a>
                    </p>
                </li>
                <li class="help-step">
                    <strong>Step 2: Go To  My Account</strong>
                    <p>
						<p>You can navigate there by selecting "My Account" from the menu. Select "You"</p>
						<img src="<?php echo $plugin_url; ?>../img/menu_button.png" class="help-img" /><br />
                        <img src="<?php echo $plugin_url; ?>../img/my_account.png" class="help-img" /><br />
                        <img src="<?php echo $plugin_url; ?>../img/account.png" class="help-img" />
                    </p>
                </li>
            </ul>
        </div>
    </div>
	<div class="help-topic">
        <h1 class="help-quest">Where can I update my billing information?</h1>
        <div class="help-detail">
            <ul>
                <li class="help-step">
                    <strong>Step 1: Go To WIO Panel and login</strong>
                    <p>
                        <p>Go to <a href="https://app.webimageoptimizer.com">https://app.webimageoptimizer.com</p>
                        <img src="<?php echo $plugin_url; ?>../img/login.png" class="help-img" /></a>
                    </p>
                </li>
                <li class="help-step">
                    <strong>Step 2: Go To  My Account</strong>
                    <p>
						<p>You can navigate there by selecting "My Account" from the menu. Select "Billing"</p>
						<img src="<?php echo $plugin_url; ?>../img/menu_button.png" class="help-img" /><br />
                        <img src="<?php echo $plugin_url; ?>../img/my_account.png" class="help-img" /><br />
                        <img src="<?php echo $plugin_url; ?>../img/billing.png" class="help-img" />
                    </p>
                </li>
            </ul>
        </div>
    </div>

</div>
