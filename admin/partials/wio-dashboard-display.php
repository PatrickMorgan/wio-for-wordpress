<?php
/**
 * Provide a meta box view for the settings page
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/admin/partials
 */

/**
 * Meta Box
 *
 * Renders a single meta box.
 *
 * @since       1.0.0
*/
?>
<?php
	//vars
	global $wpdb;

?>
<div class="clearfix" id="dashboard">
			<?php
			if($wio->authed){
				$client = $wio->get_client_info();
				$site = $client->site;
//				var_dump($wpdb);
				$img_total = (int)$wio->get_images($wpdb)["count"];
				$activity_log = $wio->get_logs();

				switch($client->status){
					case 3:
						$acct_status_class = "err";
						$acct_status = "SUSPENDED";
					break;
					case 1:
						$acct_status_class = "win";
						$acct_status = "OK";
					break;
				}

				switch($site->subscription_type){
					case 2:
						$sub_type = "FREE";
						$sub_class = "";
					break;
					case 1:
						$sub_type = "PAID";
						$sub_class = "win";
					break;
				}


				switch($site->scanning){
					case 0:
						$scan_txt = "SCAN SITE NOW";
						$scan_class = "info link_btn";
					break;
					case 1:
						$scan_txt = "SCANNING SITE FOR IMAGES";
						$scan_class = "win link_btn disabled busy";
					break;
				}
				$scan_id = "scan_btn";

				$nixtime = strtotime("now");
				if(($site->last_scanned + $wio->scan_timeout) > $nixtime){
					$scan_txt = $wio->scan_txt;
					$scan_class = "dark link_btn disabled";
					$scan_id = "scan_btn_disabled";
					$scan_disabled = "SCAN LIMIT ";
				}else{
					$scan_ok = 1;
				}
			?>
			<div class="acct">
				<h2>ACCOUNT & SITE STATUS
				<hr></h2>
				<div class="wio_msgs first <?php echo $acct_status_class; ?>">
					<p class="msg-txt"><span class="dark-txt">ACCOUNT [<?php echo $acct_status; ?>]</span> <?php echo strtoupper($client->email); ?></p>
				</div>

				<?php
				if($wio->has_subscription){
				?>
				<div id="acct_msg" class="wio_msgs <?php echo $sub_class; ?>">
					<p class="msg-txt"><span class="dark-txt">SUBSCRIPTION TYPE</span> <?php echo $sub_type; ?></p>
				</div>
				<?php
				}

				?>
				<div id="<?php echo $scan_id; ?>" class="wio_msgs <?php echo $scan_class; ?>">
					<p class="msg-txt"><span class="dark-txt"><?php echo $scan_disabled; ?></span><?php echo $scan_txt; ?></p>
				</div>
			</div>

			<div class="stats">
				<h2>OPTIMIZATION STATS
				<hr></h2>
				<div class="stat">
					<h2 class="count cash"><?php echo (int)$img_total; ?></h2>
					<h2>Total Images</h2>
				</div>
				<div class="stat">
					<h2 class="count cash"><?php echo (int)$site->stats->imgs_opt; ?></h2>
					<h2>Images Optimized By WIO</h2>
				</div>
				<div class="stat">
					<h2 class="count cash"><?php echo (int)$site->stats->in_q; ?></h2>
					<h2>Images In Queue</h2>
				</div>
				<?php
					if((int)$site->stats->in_q > 0){
						$percentage = ($site->stats->in_q/$img_total)*100;
						$queue_percentage = 100 - number_format($percentage, 2, '.', '');
					}else{
						$queue_percentage = 100;
					}
				?>
				<div class="wio-progress-wrap wio-progress" data-progress-percent="25">
				  <div style="width:<?php echo ($queue_percentage < 99.1)?$queue_percentage:99; ?>%" class="wio-progress-bar wio-progress stripes">
					  <div class="wio-progress-text"><?php echo ($queue_percentage < 100)?$queue_percentage.'% OF WEB IMAGES HAVE BEEN OPTIMIZED':"WEB IMAGE OPTIMIZATION IS COMPLETE!"; ?></div>
				  </div>
				</div>
			</div>
			<div id="activity_log" class="logs">
				<h2>RECENT ACTIVITY
				<hr></h2>
				<div class="log-list">
					<div class="log-item">
						<div class="log-txt"><strong>URL</strong></div>
						<div class="log-action"><strong>ACTION</strong></div>
						<div class="log-time"><strong>TIME</strong></div>
					</div>
					<?php
						foreach($activity_log as $log){

						$display_url = ($log['url'] != "")?$site->url.".../".end(explode('/',$log['url'])):$site->url;
						$url = $log['url'];

						$optinfo = "";
						if(isset($log["size"]["opt"])){
							$optinfo = "<br /><strong>".$log["size"]["orig"]." ~> <span style='color:green'>".$log["size"]["opt"]."</span></strong>";
						}

						$template = '<div class="log-item">'
										.'<div class="log-txt"><a target="_blank" href="'.$url.'" title="'.$url.'">'.$display_url.'</a></div>'
										.'<div class="log-action">'.$log["action"].$optinfo.'</div>'
										.'<div class="log-time">'.$log["ago"].'</div>'
										.'<hr>'
									.'</div>';

						echo $template;
						}
					?>
				</div>
			</div>
			<?php
//				echo "<small style='font-size:9px'><pre>";
//				var_dump($site);
//
//				echo "</pre></small>";
			}else{
				include(__DIR__."/wio-not-authed-display.php");
			}

			?>

</div>
