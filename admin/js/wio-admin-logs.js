(function(){
	WIO.initFuncs.push("logs");
})();

WIO.logs = {
	init:function(){
		this.tableInit();
	},
	angular:function($scope){
		$scope.logsList = function(){
			return logsList;
		}
	},
	tableInit:function(){
		jQuery('#logs_list').dataTable( {
		   "data": logsList,
	    	"order": [[ 3, "desc" ]],
	    	"language":{
	    		"emptyTable":     "No activity logs yet.",
				"info":           "Showing _START_ to _END_ of _TOTAL_ logs",
				"infoEmpty":      "Showing 0 to 0 of 0 logs",
				"infoFiltered":   "(filtered from _MAX_ total logs)",
				"lengthMenu":     "Show _MENU_ logs",
	    	},
		    "columnDefs": [
			    {
			    	"render": function ( data, type, row ) {
			    		var url = (row.url)?row.url:row.domain;
			    		var btn = "<a href='"+url+"' target='_blank'>"+url+"</a>";
		                return btn;
		            },
			    	"targets":[0],
			    	"sortable":false
			    },
			    {
			    	"render": function ( data, type, row ) {
			    		if(row.size){
							return row.action+" <br />"
								  +"<strong>"+row.size.orig+" ~> <span style=\"color:green\">"+row.size.opt+"</span></strong>";
						}else{
							return row.action;
						}
		            },
			    	"targets":[1],
			    	"sortable":true
			    },
			    {
			    	"render": function ( data, type, row ) {
		                return row.ago;
		            },
			    	"targets":[2],
			    	"sortable":true
			    },
			    {
			    	"render": function ( data, type, row ) {
		                return row.time;
		            },
			    	"targets":[3],
			    	"sortable":true,
					"visible":false
			    }
			],
		    "columns": [
		        { "title": "URL" ,"class":"url"},
		        { "title": "Action"},
		        { "title": "Time","class":"opt" },
		        { "title": "Real Time"},
		    ]
		});
	}
}
