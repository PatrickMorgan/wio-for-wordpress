(function(){
	WIO.initFuncs.push("bkup");
})();

WIO.bkup = {
	init:function(){
		this.tableInit();
	},
	angular:function($scope){
		$scope.bkupList = function(){
			return bkUpList;
		}
	},
	restoreBkup:function(img){
		var run = function(img){
			var loading = swal({
				title: "Restoring Image...",
				text: "We Are Attempting To Restore The Orginal Image",
				showConfirmButton: false
			});
			jQuery.post(WIO._loc,
				{_t:WIO._tkn,_s:WIO._sec,_ss:WIO._ssec,_ii:img,_a:'mvbkup'},
				function(rtn){
					if(rtn.restored){
						swal({
							title:"Success!",
							text:"<a href='"+WIO._uloc+"/wio-backup/_imgs/"+img+"' target='_blank'>"+img+"</a> Has Been Restored",
							html:true,
							type:"success"
						},function(){
							window.location = window.location;

						});
					}else{
						swal({
							title:"Restore Failed",
							text:rtn.msg,
							type:"error"
						});
					}
				}
			);
		}

		swal(
		{
			title: "Restore Original Image",
			text: "Restore <a href='"+WIO._uloc+"/wio-backup/_imgs/"+img+"' target='_blank'>"+img+"</a> <br /> Image will be hidden from future optimization scans <br />& removed from backup list.",
			showCancelButton: true,
			html:true,
			confirmButtonColor: "#68A6CC",
			confirmButtonText: "Restore Original Image",
			closeOnConfirm: false
		}, function(){
			run(img);
		});
	},
	rmBkUp:function(img){

		var run = function(img){
			var loading = swal({
				title: "Deleting Backup...",
				text: "We Are Attempting To Remove The Image",
				showConfirmButton: false
			});
			jQuery.post(WIO._loc,
				{_t:WIO._tkn,_s:WIO._sec,_ss:WIO._ssec,_ii:img,_a:'rmbkup'},
				function(rtn){
					if(rtn.deleted){
						swal({
							title:"Success!",
							text:"Backup Deleted.",
							html:true,
							type:"success"
						},function(){
							window.location = window.location;

						});
					}else{
						swal({
							title:"Delete Failed",
							text:rtn.msg,
							type:"error"
						});
					}
				}
			);
		}

		swal(
		{
			title: "Delete Backup Image",
			text: "Delete Backup Image <a href='"+WIO._uloc+"/wio-backup/_imgs/"+img+"' target='_blank'>"+img+"</a> & remove from list.",
			showCancelButton: true,
			html:true,
			confirmButtonColor: "#68A6CC",
			confirmButtonText: "Delete Backup",
			closeOnConfirm: false
		}, function(){
			run(img);
		});
	},
	tableInit:function(){
		jQuery('#backup_list').dataTable( {
		   "data": bkUpList,
	    	"order": [[ 3, "desc" ]],
	    	"language":{
	    		"emptyTable":     "No images backed up yet.",
				"info":           "Showing _START_ to _END_ of _TOTAL_ images",
				"infoEmpty":      "Showing 0 to 0 of 0 images",
				"infoFiltered":   "(filtered from _MAX_ total images)",
				"lengthMenu":     "Show _MENU_ images",
	    	},
		    "columnDefs": [
			    {
			    	"render": function ( data, type, row ) {
		                return row[0];
		            },
			    	"targets":[0],
			    	"sortable":true,
			    	"visible":false
			    },
			    {
			    	"render": function ( data, type, row ) {
			    		var url = row[1];
			    		var btn = "<strong style='cursor:help;color:green'>"+row[6][1]+"</strong><br />";
							btn += "<a href='"+url+"' target='_blank'>"+row[2]+"</a>";
		                return btn;
		            },
			    	"targets":[1],
			    	"sortable":true
			    },
			    {
			    	"render": function ( data, type, row ) {

			    		var ch;
						if(row.length > 6){
							if(row[6][1]){
								ch = "<span style='color:#444; font-size:0.85em; font-weight:bold'>"+row[6][0]+" ~> <span style='color:green;'>"+row[6][1]+"</span></span>";

								if(row[6].length > 2){
									if(row[6][2] === "NAN" || row[6][2] === "n/a" ){
										ch += "<br /><span style='color:#444; font-size:0.85em; font-weight:bold'>0 Bytes</span>";
									}else{
										ch += "<br /> <strong style=' font-size:0.9em;'>Saved <span style='color:green;'>"+row[6][2]+"</span>!</strong>";
									}
								}else{
									ch += "<br />pre update";
								}
							}

						}else{
							ch = "pre update";
						}
		                return ch;
		            },
			    	"targets":[2],
			    	"sortable":true
			    },
			    {
			    	"render": function ( data, type, row ) {
		                return row[5];
		            },
			    	"targets":[3],
			    	"sortable":true,
			    	"visible":true
			    },
			    {
			    	"render": function ( data, type, row ) {
			    		var btn = "<strong title='This image is using "+row[6][0]+" of storage space in wio-backup folder' style='cursor:help;'>"+row[6][0]+"</strong><br /><a href='"+WIO._url+"wp-content/uploads/wio-backup/_imgs/"+row[3]+"' target='_blank'>[VIEW]</a>";
		                return btn;
		            },
			    	"targets":[4],
			    	"sortable":true
			    },
			    {
			    	"render": function ( data, type, row ) {
			    		var btn = "<a href='javascript:WIO.bkup.restoreBkup(\""+row[3]+"\")' target='_blank'>[RESTORE]</a>";
		                return btn;
		            },
			    	"targets":[5],
			    	"sortable":false,
			    	"visible":true
			    },
			    {
			    	"render": function ( data, type, row ) {
			    		var btn = "<a href='javascript:WIO.bkup.rmBkUp(\""+row[3]+"\")' target='_blank'>[DELETE]</a>";
		                return btn;
		            },
			    	"targets":[6],
			    	"sortable":true,
			    	"visible":true
			    }
			],
		    "columns": [
		        { "title": "WP ID" },
		        { "title": "Optimized Image","class":"url"},
		        { "title": "Optimization","class":"opt" },
		        { "title": "Time","class":"opt" },
		        { "title": "Original Image","class":"bkup" },
		        { "title": "Restore","class":"actions" },
		        { "title": "Delete","class":"time-opt" }

		    ]
		});
	}
}
