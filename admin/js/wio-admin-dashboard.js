(function(){
	WIO.initFuncs.push("dashboard");
})();

WIO.dashboard = {
    init:function(){

        this.bind();
    },
    bind:function(){
        jQuery("#scan_btn").click(this.scan.confirm);
    },
    scan:{
    	request:function(){
    		//show loading
    		var loading = swal({
    			title: "Requesting Site Scan",
    			text: "Adding Site to On Demand Scan Queue...",
    			showConfirmButton: false
    		});
    		jQuery.post(WIO._loc,
    			{_t:WIO._tkn,_s:WIO._sec,_ss:WIO._ssec,_a:'rs'},
    			function(rtn){
                    console.log(rtn);
    				if(rtn.scan.req){
    					swal({
    						title:"Success!",
    						text:"Site Added to On Demand Scan Queue.",
    						type:"success",
							timer: 2000,
			    			showConfirmButton: false
    					});
						window.location = window.location;
    				}else{
    					swal({
    						title:"Request Failed",
    						text:rtn.msg,
    						type:"error"
    					});
    				}
    			}
    		);

    	},
    	confirm:function(e){
    		e.preventDefault();
    		swal(
    		{
    			title: "Request On Demand Site Scan",
    			text: "This site will be added to On Demand Scan Queue",
    			showCancelButton: true,
    			html:true,
    			confirmButtonColor: "#68A6CC",
    			confirmButtonText: "Request Scan",
    			closeOnConfirm: false
    		}, WIO.dashboard.scan.request);
    	}
    }
}
