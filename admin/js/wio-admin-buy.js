(function(){
	WIO.initFuncs.push("buy");
})();
WIO.buy = {
	creds:100,
	credVal:0.05,
	amount:"$5.00",
	init:function(){
		this.btn();
		this.bind();
	},
	btn:function(){
		var buyBtn = jQuery("#buy_btn");
		buyBtn.on("click",this.confirm);
	},
	bind:function(){
		var buyCreds = jQuery("#cred_val"),
			credAmount = jQuery("#cred_amount"),
			updateAmount = function(){
				var buyCreds = jQuery(this);
					credVal = (jQuery(this).val() > 100)?jQuery(this).val():100,
					amount = "$"+(credVal*WIO.buy.credVal).toFixed(2);
				
				if(buyCreds.val() < 100 ){
					buyCreds.val(100);
				}
				
				WIO.buy.creds = credVal;
				WIO.buy.amount = amount;
				credAmount.html(amount);
				
			};
			
		buyCreds.on("change",updateAmount);
		buyCreds.on("keyup",updateAmount);
	},
	run:function(){
		//show loading
		var loading = swal({   
			title: "Processing Purchase...",   
			text: "We Are Attempting To Charge Your WIO Account",   
			showConfirmButton: false 
		});
		jQuery.post(WIO._loc,
			{_t:WIO._tkn,_s:WIO._sec,_ss:WIO._ssec,_amt:WIO.buy.creds,_a:'bc'},
			function(rtn){
				if(rtn.added){
					swal({
						title:"Success!",
						text:rtn.creds_added+" Credits Have Been Added To Your Account <br /> New Credit Total: "+rtn.creds_total,
						html:true,
						type:"success"
					});
				}else{
					console.log(rtn);
					swal({
						title:"Payment Failed",
						text:"Please Check Your Payment Card at <a target='_blank' href='https://app.webimageoptimizer.com'>Web Image Optimizer</a>",
						html:true,
						type:"error"
					});
				}
			}
		);
		
	},
	confirm:function(e){
		e.preventDefault();
		swal(
		{   
			title: "Buy "+WIO.buy.creds+" Image Credits?",   
			text: "Your default card at <a target='_blank' href='https://app.webimageoptimizer.com'>Web Image Optimizer</a> will be charged "+WIO.buy.amount,   
			showCancelButton: true,   
			html:true,
			confirmButtonColor: "#68A6CC",   
			confirmButtonText: "Pay "+WIO.buy.amount,   
			closeOnConfirm: false 
		}, WIO.buy.run);
	}
}
