<?php
/**
 *
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    WIO_WP
 * @subpackage WIO_WP/includes
 */

/**
 * The Settings definition of the plugin.
 *
 *
 * @package    WIO_WP
 * @subpackage WIO_WP/includes
 * @author     Your Name <email@example.com>
 */
class WIO_WP_Settings_Definition {

	// @TODO: change plugin-name
	public static $plugin_name = 'wio';

	/**
	 * Sanitize plugin name.
	 *
	 * Lowercase alphanumeric characters and underscores are allowed.
	 * Uppercase characters will be converted to lowercase.
	 * Dashes characters will be converted to underscores.
	 *
	 * @access    private
	 * @return    string            Sanitized snake cased plugin name
	 */
	private static function get_snake_cased_plugin_name() {

		return str_replace( '-', '_', sanitize_key( self::$plugin_name ) );

	}

	/**
	 * [apply_tab_slug_filters description]
	 *
	 * @param  array $default_settings [description]
	 *
	 * @return array                   [description]
	 */
	static private function apply_tab_slug_filters( $default_settings ) {

		$extended_settings[] = array();
		$extended_tabs       = self::get_tabs();

		foreach ( $extended_tabs as $tab_slug => $tab_desc ) {

			$options = isset( $default_settings[ $tab_slug ] ) ? $default_settings[ $tab_slug ] : array();

			$extended_settings[ $tab_slug ] = apply_filters( self::get_snake_cased_plugin_name() . '_settings_' . $tab_slug, $options );
		}

		return $extended_settings;
	}

	/**
	 * [get_default_tab_slug description]
	 * @return [type] [description]
	 */
	static public function get_default_tab_slug() {

		return key( self::get_tabs() );
	}

	/**
	 * Retrieve settings tabs
	 *
	 * @since    1.0.0
	 * @return    array    $tabs    Settings tabs
	 */
	static public function get_tabs() {

		$tabs                = array();
		$tabs['dashboard']  = __( 'Dashboard', self::$plugin_name );
		$tabs['api_config']  = __( 'Settings & Credentials' , self::$plugin_name );
		$tabs['backup']  = __( 'BackUps', self::$plugin_name );
		$tabs['wio_logs']  = __( 'Activity Logs', self::$plugin_name );
		$tabs['help']  = __( 'Help', self::$plugin_name );

		return apply_filters( self::get_snake_cased_plugin_name() . '_settings_tabs', $tabs );
	}

	/**
	 * 'Whitelisted' WIO_WP settings, filters are provided for each settings
	 * section to allow extensions and other plugins to add their own settings
	 *
	 *
	 * @since    1.0.0
	 * @return    mixed    $value    Value saved / $default if key if not exist
	 */
	static public function get_settings() {

		$settings[] = array();

		//wio_settings section
		$config_inputs = array(
//			"api_email"=>array(
//				'name'=> __( 'Email Address',self::$plugin_name),
//				'type'=>'text'
//			),
			"api_token" =>array(
				'name'=> __( 'WIO Account Token',self::$plugin_name),
				'type'=>'text'
			),
			"api_secret"=>array(
				'name'=> __( 'WIO Account Secret',self::$plugin_name),
				'type'=>'text'
			),
			"site_secret"=>array(
				'name'=> __( 'WIO Site Secret',self::$plugin_name),
				'type'=>'text'
			),
			"backups"=>array(
				'name'=> __( 'Enable Backups',self::$plugin_name),
				'options'=>array(
					"Enable",
					"Disable"
				),
				'type'=>'select'
			)
		);

		$settings["api_config"] = $config_inputs;

		return self::apply_tab_slug_filters( $settings );
	}
}
