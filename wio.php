<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://example.com
 * @since             1.0.0
 * @package           WIO_WP
 *
 * @wordpress-plugin
 * Plugin Name:       Web Image Optimizer For Wordpress
 * Plugin URI:        http://webimageoptimizer.com/
 * Description:       A Powerful Tool that Automates the Image Optimization Process. Brought to you by the WP Management Team.
 * Version:           1.0 Beta
 * Author:            Patrick Morgan - WP Management Team
 * Author URI:        http://wpmanagementteam.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wio
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-wio-activator.php
 */
function activate_wio() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wio-activator.php';
	WIO_WP_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-wio-deactivator.php
 */
function deactivate_wio() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wio-deactivator.php';
	WIO_WP_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_wio' );
register_deactivation_hook( __FILE__, 'deactivate_wio' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wio.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_wio() {

	$plugin = new WIO_WP();
	$plugin->run();

}
run_wio();
