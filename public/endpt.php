<?php
	//
	//
	//WIO Wordpress Endpoint File
	//
	//

	define("_INCD",1);

	//check tkn & secret
	$tkn = $_REQUEST["_tkn"];
	$ssec = $_REQUEST["_ssec"];

	//if either var is not present return 404
	if(!isset($tkn) || !isset($ssec)){
		header("HTTP/1.0 404 Not Found");
	}

	//get the WP DB
//	define( 'SHORTINIT', true );
	require_once( __DIR__ . '/../../../../wp-load.php' );

	//verify key
	$option = $wpdb->get_results('SELECT * FROM '. $wpdb->prefix.'options where option_name = \'wio_settings\' ');

	$check_data = unserialize($option[0]->option_value);

	$check_token = $check_data["api_token"];
	$check_secret = $check_data["api_secret"];
	$check_site_secret = $check_data["site_secret"];
	$check_bkups = (int)$check_data["backups"];

	//check token & secret
	if($tkn != $check_token || $ssec != $check_site_secret || strlen($tkn) < 1 || strlen($ssec) < 1){
		header("HTTP/1.0 404 Not Found");
	}

	//include the WIO class
	require_once("../includes/WIO.class.php");

	$action = $_REQUEST["_act"];

	switch($action){
		case "test":
			require_once("./_endpt-bin/test.php");
		break;
		case "replace":
			require_once("./_endpt-bin/replace.php");
		break;
		case "scan":
			require_once("./_endpt-bin/scan.php");
		break;
		default:
			header("HTTP/1.0 404 Not Found");
		break;
	}

?>
