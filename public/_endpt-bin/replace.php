<?php

	//make sure its included
	if (_INCD !== 1){
		header("HTTP/1.0 404 Not Found");
		die();
	}

	//WIO
	$wio = new WIO($check_token,$check_secret,$check_site_secret);

	//vars
	$img_id = $_REQUEST["_iid"];
	$img_wid = (int)$_REQUEST["_iid"];
	$img_hash = $_REQUEST["_ihash"];

	$upload_dir = wp_upload_dir();

	$img = $wio->grab_image($img_wid)["img"];
	$img_path = $img["new_path"];

	$optimized_img_loc = $img["opt_url"];

	$bkup_img = $img_id."-".end(explode("/",$img_path));
	$bkup_loc = $upload_dir["basedir"]."/wio-backup/_imgs/{$bkup_img}";

	$img_loc = $upload_dir["basedir"].$img_path;
	$new_uri = $upload_dir["baseurl"].$img_path;

	$size = array(
		"orig"=>urldecode($_REQUEST["_sz"]),
		"opt"=>urldecode($_REQUEST["_osz"]),
		"diff"=>urldecode($_REQUEST["_chg"])
	);


	//get contents of image at loc

	@$img_data = file_get_contents($optimized_img_loc);

	if($img_data === FALSE){

  	  $rtn = array(
  	  	"ok"=>true,
  	  	"optimized"=>false,
  	  	"err"=>"not_optimized",
		"img"=>$new_uri,
  	  );
  	  header("Content-Type:application/json");
  	  $out = json_encode($rtn);

  	  die( $out);
	}

	$img = base64_encode($img_data);


	header("Content-Type:text/plain");

	//backup image
	if($check_backups == 0){
		$wio->backup_img($img_loc,$bkup_loc);
	}

	//overwrite in
	if(WIO::b64_to_img($img,$img_loc)){
			$post = get_post($img_id);
			$file = get_attached_file($img_id);

			update_attached_file( $img_id, $img_loc );
			$wio->update_opt_image($img_id,$new_uri,$img_path,$bkup_img,$size);

			include( ABSPATH . 'wp-admin/includes/image.php' );
			  $attach_data = wp_generate_attachment_metadata( $img_id, $img_loc );
			  wp_update_attachment_metadata( $img_id,  $attach_data );

		  	  $rtn = array(
		  	  	"ok"=>true,
		  	  	"replaced"=>true,
		  	  	"uri"=>$new_uri,
		  	  );
		  	  header("Content-Type:application/json");
		  	  $out = json_encode($rtn);

		  	  die( $out);
	}
