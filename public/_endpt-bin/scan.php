<?php
	//make sure its included
	if (_INCD !== 1){
		header("HTTP/1.0 404 Not Found");
		die();
	}

	$wio = new WIO($check_token,$check_secret,$check_site_secret);

	//grab images from WP DB
	$imgs = $wpdb->get_results("SELECT * FROM {$wpdb->posts} WHERE post_mime_type LIKE 'image%'");

	//create scan array
	$scan = array(
		"count"=>0,
		"uri"=> wp_upload_dir()["baseurl"],
		"imgs"=>array(),
		"plugin_dir"=>$wio->plugin_dir
	);


	$db = $wio->connect_db();

	//loop through images
	foreach($imgs as $img){

		$img_id = (int)$img->ID;

		$optimized = $db->get("opt","wp_id",$img_id);


		if($optimized == true){
			//already optimized
		}else{
			//add to scan list
			$scan["imgs"][] = array(
				"_id"=>(int)$img->ID,
				"_uri"=>WIO::get_short_uri($img->guid),
				"optimized"=>$db->get("opt","wp_id","$img_id")
			);
			$scan["count"]++;
		}
	}

	//output
	header("Content-Type:application/json");
	echo json_encode($scan,JSON_PRETTY_PRINT);
