<?php

	//make sure its included
	if (!defined(_INCD)){
		header("HTTP/1.0 404 Not Found");
		die();
	}

	$upload_dir = wp_upload_dir();

	$upload_dir = $upload_dir["basedir"];

	$write = WIO::is_writable_dir($upload_dir);

	header("Content-Type:text/plain");

	$err = "";

	//test writing of upload dir
	if($write){
		$test = "ist_gut";
	}else{
		$err .= "no_rw,";
	}

	//test for JSON
	if(json_decode(json_encode(array("test"=>"data")))){
		$test = "ist_gut";
	}else{
		$err .= "no_json,";
	}

	if(strlen($err) > 1){
		die($err);
	}

	die($test);
